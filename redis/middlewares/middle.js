import fetch from 'node-fetch';
import  {createClient}  from 'redis';

const client = createClient();


const getRepos = async (req, res, next) => {
    try {
        console.log("Fetching Data !!");
        const {username} = req.params;
        const response = await fetch(`https://api.github.com/users/${username}`);

        //promise is resolved and result is parsed and returned as json
        const data = await response.json();
        console.log(data);
        const count = data.public_repos;

        //set data to redis
        //setex(username, sec, )
        client.setex(username, 3600, count);
        res.send({
            username,
            public_repo_count: count
        })
        // res.send(data);
    } 
    catch(err) {
        console.log(err.message);
    }
}

const cache = (req,res,next) => {
    const {username} = req.params;

    //get value by the key
    client.get(username, (err,data) => {
        if(err) throw err;
        if(data !== null) {
            res.send({
                username,
                public_repo_count: data
            })
        }
        else {
            next();
        }
    });

}

export {cache, getRepos};