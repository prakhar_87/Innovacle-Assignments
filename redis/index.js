import express from 'express';
import {cache, getRepos} from './middlewares/middle.js';

const app = express();

//first check if data is present in the cache else make the fetch request
app.get('/repo/:username', cache, getRepos);

app.listen(3001, () => {
    console.log("Server is up and runnning");
})