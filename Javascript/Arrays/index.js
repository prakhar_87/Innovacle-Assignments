arr = [1,2,3];


arr.shift();
arr.unshift(4,5,6);
arr.unshift(...[0,0,0])
console.log(arr);

var ind = arr.lastIndexOf(0);
console.log(ind);

var arr2 = arr.splice(1,2);
console.log(arr2);

var cps = arr.slice(0,2);
console.log(cps);

console.log(Object.keys(arr));

//map function
let cp = arr.map((val) => val*2)
console.log(cp);

//forEach
arr.forEach((val) => console.log(val));

//sort
//converts the array elements into string and sorts lexicographically
arr.sort();
console.log(arr);

//sort in ascending order
arr.unshift(100);
arr.sort((a,b) => a-b);
console.log(arr);

//filter in array - conditioned selection
const word = ['abc', 'abcde', 'abcdef', 'ab', 'abaaaff'];
var cpw = word.filter(word => word.length > 3);
console.log(cpw);


