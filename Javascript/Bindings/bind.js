// .call .bind .apply

let person = {
    name: 'prakhar',
    getName: function() {
        console.log(this.name);
    }
};

setTimeout(person.getName, 1000);
//this will return undefined because in backend it is executing like this
//let f = person.getName;
//setTimeout(f(), 1000);

//To avoid this we can use 2 alternatives
//1) 
setTimeout(function() {
    person.getName();
}, 1000)

//2) 
let f = person.getName.bind(person);
setTimeout(f, 1000);


let runner = {
    name: 'Runner',
    age: 20,
    run: function(speed) {
        console.log(this.name + ' runs at ' + speed + ' mph.');
    }
};

let flyer = {
    name: 'Flyer',
    fly: function(speed) {
        console.log(this.name + ' flies at ' + speed + ' mph.');
    }
};

let run = runner.run.bind(flyer, 20);
run();
