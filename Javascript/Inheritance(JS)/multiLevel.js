//          Sports
//            |
//          Cricket
//            |
//          OneDay


function Sports() {
    //number of players in each team
    this.playerCount = 0; 

    //indoor or outdoor
    this.type = 'outdoor';
}

function Cricket(format, overs) {

    //properties belonging to sports can now be used in Cricket as well
    Sports.call(this);
    this.playerCount = 11;

    //formats can be : twenty, fifty, test (by-default it is test)
    this.format = format;
    this.overs = overs;
}

function OneDay(title) {
    Cricket.call(this);
    this.format = 'fifty_fifty';
    this.overs = 50;
    this.time = 9;
    this.title = title;
}
// Cricket inherits properties from Sports Prototype Object
// Cricket -> Sports

//a new object is created and it's __proto__ is references to Sports
Cricket.prototype = Object.create(Sports.prototype);
Cricket.prototype.constructor = Cricket;

//oneDay -> Cricket -> Sports
OneDay.prototype = Object.create(Cricket.prototype);
OneDay.prototype.constructor = OneDay;

//Creating an instance of OneDay 
let menOneDay = new OneDay("men's");

console.log(menOneDay.title);

//adding print statement to Sport's Prototype
Sports.prototype.print = function(comingFrom) {
    console.log(`In Sports coming from ${comingFrom}`);
}

//adding print statement to Cricket's Prototype
Cricket.prototype.print = function(comingFrom) {
    console.log(`In Cricket coming from ${comingFrom}`);
    Cricket.prototype.__proto__.print('Cricket');
}

console.log(menOneDay);
console.log(`Starting from Men's one day stadium and going up the hierarchy !`)
menOneDay.print(`men's one day stadium`);