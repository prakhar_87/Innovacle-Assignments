//        Sports
//          |
//        Cricket


function Sports() {
    //number of players in each team
    this.playerCount = 0; 
    
    //indoor or outdoor
    this.type = 'outdoor';
}

function Cricket(format, overs) {

    //properties belonging to sports can now be used in Cricket as well
    Sports.call(this);
    this.playerCount = 11;

    //formats can be : twenty, fifty, test (by-default it is test)
    this.format = format;
    this.overs = overs;
}

// Cricket inherits properties from Sports
// Cricket -> Sports

//a new object is created and it's __proto__ is references to Sports
Cricket.prototype = Object.create(Sports.prototype);
Cricket.prototype.constructor = Cricket;

//Creating an instance of Cricket 
let ipl = new Cricket('twenty-twenty', 20);

console.log(ipl.playerCount);

//adding print statement to Sport's Prototype
Sports.prototype.print = function(comingFrom) {
    console.log(`In Sports coming from ${comingFrom}`);
}

ipl.print('Cricket');