//              Sports
//              ___|___
//             |       |
//         Cricket    Basketball
//       ___|___          ___|___
//      |       |        |       |
//   OneDay   Test     NBA       FIBA

function Sports() {
    //number of players in each team
    this.playerCount = 0; 
    
    //indoor or outdoor
    this.type = 'outdoor';
}

function Cricket(format, overs) {

    //properties belonging to sports can now be used in Cricket as well
    Sports.call(this);
    this.playerCount = 11;

    //formats can be : twenty, fifty, test (by-default it is test)
    this.format = format;
    this.overs = overs;
}

function Basketball() {

    //Using existing properties of Sports
    Sports.call(this);
    this.playerCount = 5;   
    this.type = 'indoor';
    
    this.format = 'NBA'; //by default
    this.time = '48min'; //by default
}

//Cricket inheriting properties from Sports prototype object
Cricket.prototype = Object.create(Sports.prototype);
Cricket.prototype.constructor = Cricket;

//Basketball inherting properties from sports prototype object
Basketball.prototype = Object.create(Sports.prototype);
Basketball.prototype.constructor = Basketball;

function OneDay(title) {
    Cricket.call(this);
    this.format = 'fifty_fifty';
    this.overs = 50;
    this.time = '9hrs'; //in hrs
    this.title = title; //men's
}

function Test(title) {
    Cricket.call(this);
    this.format = 'test';
    this.overs = 90;
    this.time = '5days'; //in days
    this.title = title; //ashes
}

//oneDay -> Cricket -> Sports
OneDay.prototype = Object.create(Cricket.prototype);
OneDay.prototype.constructor = OneDay;
let menOneDay = new OneDay("men's");
console.log(menOneDay);

//Test -> Cricket -> Sports
Test.prototype = Object.create(Cricket.prototype);
Test.prototype.constructor = Test;
let womensTest = new Test("women's");
console.log(womensTest);

function NBA(title) {
    Basketball(this);
    this.format = 'NBA';
    this.time = '48min';
    this.quaters = 4;
    this.title = title;
}

function FIBA(title) {
    Basketball(this);
    this.format = 'FIBA';
    this.time = '40min';
    this.quaters = 4;
    this.title = title;
}

//NBA -> Basketball -> Sports
NBA.prototype = Object.create(Basketball.prototype);
NBA.prototype.constructor = NBA;
let mensNba = new NBA("men's");
console.log(mensNba);

//FIBA -> Basketball -> Sports
FIBA.prototype = Object.create(Basketball.prototype);
FIBA.prototype.constructor = FIBA;
let womenFIBA = new FIBA("women's");
console.log(womenFIBA);

//defining print functions 
Sports.prototype.print1 = function(comingFrom) {
    console.log(`In Sports coming from ${comingFrom}`);
}

Cricket.prototype.print2 = function(comingFrom) {
    console.log(`In Cricket coming from ${comingFrom}`);
    Cricket.prototype.__proto__.print('Cricket');
}

Basketball.prototype.print3 = function(comingFrom) {
    console.log(`In Basketball coming from ${comingFrom}`);
    Basketball.prototype.__proto__.print('Basketball');
}

console.log(`Starting from Womens's FIBA stadium and going up the hierarchy !`)
womenFIBA.print1(`women's FIBA Stadium`);



