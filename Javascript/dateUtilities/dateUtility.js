var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thrusday', 'Friday', 'Saturday'];

function diff(date1, date2, param) {
    var milisec = Math.abs(date1.getTime() - date2.getTime()); //in miliseconds;
    var noOfDays = Math.ceil(milisec/param); 
    return noOfDays;
}

function addMonths(currDate, noOfMonths) {
    //year automatically adjusted
    return new Date(currDate.setMonth(currDate.getMonth() + noOfMonths)); 
}

function addHours(currDate, noOfHours) {
    return new Date(currDate.setHours(currDate.getHours()+noOfHours));
}

//similary addMinutes and addSeconds can be implemented

function getFirstDay(currDate) {
    //point to month start of the current date
    var firstDate = new Date(currDate.getFullYear(), currDate.getMonth(), 1);
    var day = days[firstDate.getDay()];
    return day;
}

function getLastDay(currDate) {
    //point to month start of the current date
    var lastDate = new Date(currDate.getFullYear(), currDate.getMonth() + 1, 0);
    var day = days[lastDate.getDay()];
    return day;
}

function getDays(currDate) {
    var lastDate = new Date(currDate.getFullYear(), currDate.getMonth() + 1, 0);
    return lastDate.getDate();
}

module.exports = { diff, addMonths, getFirstDay, getLastDay, addHours, getDays};



