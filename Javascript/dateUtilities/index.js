//initializing 2 random dates
//mm/dd/yyyy

const utility = require('./dateUtility.js');

//Difference : diff(date1, date2);
var date1 = new Date('11/20/2019');
var date2 = new Date('11/20/2018');
var diffInDays = 24*60*60*1000;
var diffInHours = 60*60*1000;
var diffInMin = 60*1000;
var ans1 = utility.diff(date1, date2, diffInDays);
var ans2 = utility.diff(date1, date2, diffInHours);
var ans3 = utility.diff(date1, date2, diffInMin);
console.log(`
    Difference in Days : ${ans1}
    Difference in Hours : ${ans2}
    Difference in Minutes : ${ans3}
`);

//Add Months : addMonths(current date, no. of months);
var ans;
var noOfMonths = 24;
var currDate = new Date();
ans = utility.addMonths(currDate, 12);
console.log(ans.toString());

//Add Hours : addHours(current date, no of hours);
currDate = new Date();
ans = utility.addHours(currDate, 4);
console.log(ans.toString());

//First Day of the month of the given date : getFirstDay(date);
var date = new Date('2/2/2014');
ans = utility.getFirstDay(date);
console.log(ans);

//Last Day of the month of the given date : getLastDay(date);
date = new Date('3/2/2014');
ans = utility.getLastDay(date);
console.log(ans);

//get days in a month
date = new Date('3/3/2020');
ans = utility.getDays(date);
console.log(ans);




