// //takes 1 sec to get resolved
timer1 = () => {
    return new Promise(res => {
        setTimeout(() => {
            res('fast timer1');
        }, 1000);
    })
}

//takes 2 sec to get resolved
timer2 = () => {
    return new Promise(res => {
        setTimeout(() => {
            res('slow timer2');
        }, 2000);
    })
}

sequential = async() => {
    
    const slow = await timer2();
    console.log(slow);
    //after 2 sec

    const fast = await timer1();
    console.log(fast);
    //after 3 sec, since it waited for timer2 for 2 sec

}

concurrent = async() => {
    const slow = timer2();
    const fast = timer1();

    //both started simultaneously
    console.log(await slow); //completed in 2 sec
    console.log(await fast); //completed in 2 sec 
}

allPromise = () => {
    return Promise.all([timer1(), timer2()]);
}

parallelPromise = async() => {
    await Promise.all([
        //self-calling functions
        (async() => console.log(await timer2()))(),
        (async() => console.log(await timer1()))()
    ])
}

//calling functions -> 

// sequential();
// concurrent();
// parallelPromise();

const getVal = allPromise();
getVal.then((data) => {
    console.log(data[0]);
    console.log(data[1]);
})





