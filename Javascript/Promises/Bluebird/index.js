var Promise = require('bluebird');

// var newPromise = new Promise((res, rej) => {
//     res("Resolved !!")
// })

// newPromise.
// then((val) => {
//     console.log(val);
// })
// .catch(err => {
//     console.log(err);
// })


// .coroutine returns a function that use yield to yield Promises

var cnt = 0;
const fun1 = Promise.coroutine(function* (val) {
    console.log(`In fun1 at ${cnt}`);
    yield Promise.delay(1000);
    yield fun2(cnt++);
    console.log('Hi');
})

const fun2 = Promise.coroutine(function* (val) {
    console.log(`In fun2 at ${cnt}`);
    yield Promise.delay(1000);
    fun1(cnt++);
})

fun1(0);






