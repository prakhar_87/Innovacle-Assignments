const async = require('async');

const arr = [5,4,3,2];
const responses = [];

async.forEachOfLimit(arr, 1, (value, index,callback) => {
    setTimeout(() => {
        responses[index] = ` Visited at index ${index} with value ${value}`;
        callback();
    }, 1000 * index);
})
.then(() => {
    console.log(responses);
})
.catch(err => console.log(err))