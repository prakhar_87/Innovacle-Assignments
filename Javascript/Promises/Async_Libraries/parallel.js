const async = require('async');

async.parallel([
    async.reflect(async () => await Promise.resolve('Hello')),
    async.reflect(async () => await new Promise(res => {
        setTimeout(() => {
            res('Resolves Finally !')
        }, 1000)
    })),
    async.reflect(async() => {
            // try{
            //     await Promise.reject('Rejected')
            // }
            // catch(err) {
            //     console.log(err);
            // }
            await Promise.reject('Rejected')
        }
    )
])
.then(res => {
    console.log(res);
})
.catch(err => console.log(err))
