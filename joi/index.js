const express = require('express');
const {authSchema} = require('./schema.js')
const app = express();

app.use(express.json());

app.post('/register', async(req,res,next) => {
    try {
        const {email, password} = req.body;
        const check = await authSchema.validateAsync(req.body);
        // console.log(check);
    }
    catch(err) {
        res.json({
            message: err.message
        })
    }
})

app.listen(3000, ()=> {
    console.log('Server is up and running');
})