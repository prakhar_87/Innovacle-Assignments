// const argon2 = require('argon2');
// const express = require('express');
// const app = express();

// app.use(express.json());

// app.post('/registration', async (req, res) => {
//     let hash = await argon2.hash(req.body.password, {type: argon2.argon2id});
//     console.log(`Hash: ${hash}`);
// })

// let port = 3006;
// app.listen(port, 'localhost', () => {
//     console.log('Server is up and running');
// })

const express = require('express');
const { nextTick } = require('process');
const app = express();

console.log('Run everytime');

app.use(express.json());

app.get('/home', (req, res, next) => {
    console.log("Hello World !!");
    next();
})

let port = 3006;
app.listen(port, 'localhost', () => {
    console.log('Server is up and running');
})