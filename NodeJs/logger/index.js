const express = require('express');
const app = express();
const logger = require('./winston.js');
const morgan = require('morgan');

//format
app.use(morgan('tiny'));

//tokens    
// app.use(morgan(':method:status:url"HTTP/:http-version"'));

// morgan(function (req, res) {
//     return req.method + ' ' + req.url + ' ' + req.uuid;
// })

// app.use(morgan("combined", { stream: logger.stream.write }));


// app.use((req, res, done) => {
//     // logger.info(req.originalUrl);
//     done();
// });

app.get('/', (req,res) => {
    logger.info('server.home.beginning');
})

logger.error("error");
logger.warn("warn");
logger.info("info");
logger.info("info2");
//initially till info will be printed because 
//winston logger by default has level set to info
//so logs with info and higher priority will be printed
//we can change that level
logger.verbose("verbose");
logger.debug("debug");
logger.silly("silly");

port = 3010;
app.listen(port, 'localhost', () => {
    console.log(`Server is up and running`);
})

