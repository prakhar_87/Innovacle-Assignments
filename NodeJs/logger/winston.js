const {format, createLogger, transports } = require('winston');

//defining a custom format
const customFormat = format.combine(format.timestamp(), format.printf((info) => {
    return `${info.timestamp} - [${info.level.toUpperCase().padEnd(7)}] - ${info.message}`
}))

const logger = createLogger({
    //here we can define our own format of printing logs
    format: customFormat,
    //defines the level till which the logs should be printed
    level: 'debug',
    //specifies where the logs should be shown 
    transports: [
        new transports.Console({level: 'debug'}),
        //if log file reaches specific limit it deletes the old one and creates new ones
        new transports.File({filename: 'app.log', level: 'info'})
    ],

    exitOnError: false
})

module.exports = logger;