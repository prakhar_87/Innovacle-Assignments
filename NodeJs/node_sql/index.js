const express = require('express');
const app = express();
const conn = require('./connection.js');
const bcrypt = require('bcryptjs');

const port = 3001;

//parses incoming request with json payload
app.use(express.json());

//Get all rows present in the database table POST
app.get('/posts', (req, res) => {
    conn.query(`SELECT * FROM POST`, (err,result) => {
        if(err) throw err;
        else res.json(result);
    })
})

app.get('/posts/:id', (req, res) => {
    conn.query(`SELECT * FROM POST WHERE id = ${req.params.id}`, (err,result) => {
        if(err) throw err;
        res.json(result);
    })
})

app.post('/posts', async (req,res) => {
    //Suppose I want to hash the title;
    let title = req.body.unique_id;
    const hash = await bcrypt.hash(title, 10);
    req.body.unique_id = hash;
    res.status(200).json("Posted Successfully !");
    conn.query(`INSERT INTO POST SET ?`, req.body, (err, result) => {
        if(err) throw err;
        console.log(
            `Data successfully inserted !! \n Number of records inserted are ${result.affectedRows}`
        );
    })
})

app.delete('/posts', (req, res) => {
    let id = req.body.id;
    conn.query(`DELETE FROM POST WHERE id = ${id}`, (err,result) => {
        if(err) throw err;
        else console.log(result);
    })
})

app.put('/posts', (req,res) => {
    conn.query(`UPDATE POST SET author = ? where id = ?`, [req.body.author, req.body.id], (err, result) => {
        if(err) throw err;
        res.send(JSON.stringify(result));
    })
})

app.listen(port, 'localhost', () => {
    console.log(`Server is up and running at the port http://localhost:${port}`);
})
