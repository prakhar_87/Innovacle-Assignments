const bcrypt = require('bcrypt');
const express = require('express');
const app = express();
const conn = require('./db.js');

app.use(express.json());

//encryption
app.post('/registration', async (req,res) => {
    let {email,password} = req.body;
    //salt round is a cost factor defines the round of hashing done
    let saltround = 10;
    let secure = await bcrypt.hash(password, saltround);
    req.body.password = secure;
    conn.query(`INSERT INTO USERDETAILS SET ?`, req.body, (err, result) => {
        if(err) throw err;
        console.log(
            `Data successfully inserted !! \n Number of records inserted are ${result.affectedRows}`
        );
    })
})

//validation
app.post('/login', (req,res) => {
    let {email,password} = req.body;
    conn.query(`SELECT * FROM USERDETAILS WHERE email = '${email}'`, async(err, result) => {
        if(err) throw err;
        let x = await bcrypt.compare(password, result[0].password);
        if(x) {
            console.log(x);
            res.send('Login Successful !!');
        } else {
            res.send('Incorrect email /password');
        }
    })
})

let port = 3002;
app.listen(port , 'localhost', ()=> {
    console.log(`Server is listening at at the port http://localhost:${port}`);
})
