const http = require('http');
  
// Creating the server
const server = http.createServer((req, res) => {
  
    if (req.url === '/') {
        res.write(`
            <html>
                <head><title>Calculator</title><head>
                <body>
                    <form action="/multiply" method="POST">
                        Number 1 : 
                        <input type="text" name="num1"></input><br /><br />
                        Number 2 : 
                        <input type="text" name="num2"></input>
                        <br /><br />
                        <button formaction="/sum" type="submit">Sum</button>
                        <button formaction="/multiply" type="submit">Multiply</button>
                        <button formaction="/subtraction" type="submit">Subtraction</button>
                        <button formaction="/division" type="submit">Division</button>
                    </form>
                </body>
            </html>`
        );
        res.end();
    }
  
    if (req.url === '/sum' && req.method === 'POST') {
        const body = [];
  
        req.on('data', (chunk) => {
            body.push(chunk);
            console.log(body);
        });
  
        req.on('end', () => {
            //Data is received in chunks and stored as buffer
            //Buffer array is concatenated and converted to string
            const parsedBody = Buffer.concat(body).toString();
            console.log(parsedBody);

            //string of this type is received 
            //num1=5&num2=5
            //splitting it with '&' and '=' operators 
            let num1 = parsedBody.split('&')[0].split('=')[1];
            let num2 = parsedBody.split('&')[1].split('=')[1];

            //converting string to number
            num1 = parseInt(num1);
            num2 = parseInt(num2);
            console.log(typeof num1);

            res.writeHead(200);
            //Sending the response in form of String
            res.end(String(num1+num2));
        });
    }

    if (req.url === '/multiply' && req.method === 'POST') {
        const body = [];
  
        req.on('data', (chunk) => {
            body.push(chunk);
            console.log(body)
        });
  
        req.on('end', () => {
            //Data is received in chunks and stored as buffer
            //Buffer array is concatenated and converted to string
            const parsedBody = Buffer.concat(body).toString();
            console.log(parsedBody);

            //string of this type is received 
            //num1=5&num2=5
            //splitting it with '&' and '=' operators 
            let num1 = parsedBody.split('&')[0].split('=')[1];
            let num2 = parsedBody.split('&')[1].split('=')[1];

            //converting string to number
            num1 = parseInt(num1);
            num2 = parseInt(num2);
            console.log(typeof num1);

            res.writeHead(200);
            //Sending the response in form of String
            res.end(String(num1*num2));
        });
    }

    if (req.url === '/division' && req.method === 'POST') {
        const body = [];
  
        req.on('data', (chunk) => {
            body.push(chunk);
            console.log(body)
        });
  
        req.on('end', () => {
            //Data is received in chunks and stored as buffer
            //Buffer array is concatenated and converted to string
            const parsedBody = Buffer.concat(body).toString();
            console.log(parsedBody);

            //string of this type is received 
            //num1=5&num2=5
            //splitting it with '&' and '=' operators 
            let num1 = parsedBody.split('&')[0].split('=')[1];
            let num2 = parsedBody.split('&')[1].split('=')[1];

            //converting string to number
            num1 = parseInt(num1);
            num2 = parseInt(num2);
            console.log(typeof num1);

            res.writeHead(200);
            //Sending the response in form of String
            res.end(String(num1/num2));
        });
    }

    if (req.url === '/subtraction' && req.method === 'POST') {
        const body = [];
  
        req.on('data', (chunk) => {
            body.push(chunk);
            console.log(body)
        });
  
        req.on('end', () => {
            //Data is received in chunks and stored as buffer
            //Buffer array is concatenated and converted to string
            const parsedBody = Buffer.concat(body).toString();
            console.log(parsedBody);

            //string of this type is received 
            //num1=5&num2=5
            //splitting it with '&' and '=' operators 
            let num1 = parsedBody.split('&')[0].split('=')[1];
            let num2 = parsedBody.split('&')[1].split('=')[1];

            //converting string to number
            num1 = parseInt(num1);
            num2 = parseInt(num2);
            console.log(typeof num1);

            res.writeHead(200);
            //Sending the response in form of String
            res.end(String(num1-num2));
        });
    }
});
  
// Starting the server in listening mode;
server.listen(3080,'localhost');    

