const express = require('express');
const app = express()
const port = 3000

//parses the incoming request with json payloads
app.use(express.json());

app.get('/sum', (req, res) => {
    let [num1, num2] = [parseInt(req.query.num1), parseInt(req.query.num2)];
    res.send(String(num1+num2));
})

app.get('/sum/:expression', (req, res) => {
    // let [num1, num2] = [parseInt(req.query.num1), parseInt(req.query.num2)];
    let data = req.params.expression;
    let [num1, num2] = [parseInt(data.split('+')[0]), parseInt(data.split('+')[1])];
    res.send(String(num1+num2));
})

app.get('/multiply', (req, res) => {
    let [num1, num2] = [parseInt(req.query.num1), parseInt(req.query.num2)];
    res.send(String(num1*num2));
})

app.get('/multiply/:expression', (req, res) => {
    // let [num1, num2] = [parseInt(req.query.num1), parseInt(req.query.num2)];
    let data = req.params.expression;
    let [num1, num2] = [parseInt(data.split('*')[0]), parseInt(data.split('*')[1])];
    res.send(String(num1*num2));
})

app.get('/subtraction', (req, res) => {
    let [num1, num2] = [parseInt(req.query.num1), parseInt(req.query.num2)];
    res.send(String(num1-num2));
})

app.get('/subtraction/:expression', (req, res) => {
    // let [num1, num2] = [parseInt(req.query.num1), parseInt(req.query.num2)];
    let data = req.params.expression;
    let [num1, num2] = [parseInt(data.split('-')[0]), parseInt(data.split('-')[1])];
    res.send(String(num1-num2));
})

app.get('/divide', (req, res) => {
    let [num1, num2] = [parseInt(req.query.num1), parseInt(req.query.num2)];
    res.send(String(num1/num2));
})

app.get('/subtraction/:expression', (req, res) => {
    // let [num1, num2] = [parseInt(req.query.num1), parseInt(req.query.num2)];
    let data = req.params.expression;
    let [num1, num2] = [parseInt(data.split('/')[0]), parseInt(data.split('/')[1])];
    res.send(String(num1/num2));
})

app.post('/sum', (req,res) => {
    let [num1, num2] = [parseInt(req.body.num1), parseInt(req.body.num2)];
    res.send(String(num1+num2));
})

app.post('/multiply', (req,res) => {
    let [num1, num2] = [parseInt(req.body.num1), parseInt(req.body.num2)];
    res.send(String(num1*num2));
})

app.post('/divide', (req,res) => {
    let [num1, num2] = [parseInt(req.body.num1), parseInt(req.body.num2)];
    res.send(String(num1/num2));
})

app.post('/subtraction', (req,res) => {
    let [num1, num2] = [parseInt(req.body.num1), parseInt(req.body.num2)];
    res.send(String(num1-num2));
})

app.listen(port, 'localhost', () => {
  console.log(`Server is up and running at http://localhost:${port}`)
})