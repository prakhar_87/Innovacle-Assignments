const http = require('http');

const server = http.createServer((req, res) => {
    if(req.url === '/') {
        res.end("In Home Page");
    }

    else if(req.url === '/about') {
        res.end("In About Page");
    }

    else if(req.url === '/contact') {
        res.end("In contact Page");
    }

    else {
        //404: Page not found !
        res.writeHead(404, {"Content-type": "text/html"});
        res.end("<h2>Error 404 !!</h2>");
    }
})

//localhost(default) = 127.0.0.1
//port number = 3000
//default port number for hhtp = 80
//default port number for https = 443
server.listen(3000, 'localhost', () => {
    console.log(`Server is up and listening to port number 3000 !`)
});
