const http = require('http');

//http.createServer() method includes callback with response and request 
//parameter provided by Node.js

//req: info about current http request
//createServer with a request listener function each time server gets a http request
const server = http.createServer((req, res) => {
    if(req.url === '/') {
        res.write('Hello World');
        res.end();
    }

    //route
    if(req.url === '/api/array') {
        res.write(JSON.stringify([1,2,3,4]));
        res.end();
    }
})

//localhost = 127.0.0.1
server.listen(3000, 'localhost', () => {
    console.log(`listening to port number 3000 !`)
});
