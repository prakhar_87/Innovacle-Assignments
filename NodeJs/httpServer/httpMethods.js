const http = require('http');
let fs = require('fs');

const server = http.createServer((req, res) => {

    if(req.url === '/') {
        if(req.method === 'GET') {
            res.writeHead(200, {"cotent-type": "text/html"});
            //.pipe() attaches a writable stream to a readable stream
            fs.createReadStream('./public/index.html', 'UTF-8').pipe(res); 
        }
        else if(req.method === 'POST') {
            console.log("In Post method");
            let data = "";
            req.on("data", (chunk) => {
                data += chunk;
            })
            req.on("end", () => {
                res.writeHead(200, {"content-type": "text/html"});
                res.end(`${data} - - - data received`);
            })
        }
    }
})

server.listen(3080, () => {
    console.log("Server is up and listening to port number 3080");
});


