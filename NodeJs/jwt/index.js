const bcrypt = require('bcrypt');
const express = require('express');
const jwt = require('jsonwebtoken');
const app = express();
const conn = require('./db.js');

app.use(express.json());

//encryption
app.post('/registration', async (req,res) => {
    let {password} = req.body;
    //salt round is a cost factor defines the round of hashing done
    let saltround = 10;
    let secure = await bcrypt.hash(password, saltround);
    req.body.password = secure;
    conn.query(`INSERT INTO USER SET ?`, req.body, (err, result) => {
        if(err) throw err;
        console.log(
            `Data successfully inserted !! \n Number of records inserted are ${result.affectedRows}`
        );
    })
})

//validation
app.post('/login', (req,res) => {
    let {name,email,password} = req.body;
    let data = {
        name: name,
        email: email
    }
    conn.query(`SELECT * FROM USER WHERE email = '${email}'`, async(err, result) => {
        if(err) throw err;
        let x = await bcrypt.compare(password, result[0].password);
        if(x) {
            console.log(x);
            // res.send('Login Successful !!');
            jwt.sign({user: data}, "secretkey" , { expiresIn: "2d" }, (err,token) => {
                res.json({
                    ...req.body,
                    token
                })
            })
        } else {
            res.send('Incorrect email /password');
        }
    })
})

app.post('/posts', verifyToken, async (req,res) => {
    jwt.verify(req.token, "secretkey" , (err, payload) => {
        if(err) {
            res.sendStatus(403);
        }//unauthorized
        
        conn.query(`INSERT INTO POST SET ?`, req.body, (err, result) => {
            if(err) throw err;
            console.log(
                `Data successfully inserted !! \n Number of records inserted are ${result.affectedRows}`
            );
        })

        res.json({
            message: "Post Created Successfully",
            payload
        })
    })
})

//verify the token
function verifyToken(req, res, next) {
    const header = req.headers['authorization'];
    if(typeof header !== 'undefined') {
        const _token = header.split(" ")[1];
        req.token = _token;
        // req.token = header;
        next();
    }
    else {
        res.sendStatus(403); //forbidden
    }
}

let port = 3005;
app.listen(port , 'localhost', ()=> {
    console.log(`Server is listening at at the port http://localhost:${port}`);
})
