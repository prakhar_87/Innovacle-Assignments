"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var message = "Hello User !";
console.log(message);
var x = 10;
var consition = true;
var sum = 20;
var name = 'prakhar';
var sent = "My name is " + name + "\nBtech CSE IIITN";
console.log(sent);
var n = null;
var u = undefined;
var myName = undefined;
//Arrays
var list1 = [1, 2, 3];
var list2 = [1, 2, 3];
var mixed = ['string', 10];
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 12] = "Green";
    Color[Color["Blue"] = 13] = "Blue";
})(Color || (Color = {}));
;
//Red: 0 , Green: 1, Blue: 3
var c = Color.Blue;
console.log(c);
var randomValue = 10;
randomValue = true;
randomValue = 'Prakhar';
var list3 = ['prakhar', 23, true];
console.log(list3);
//any can be called as a funtion or an object or anything
var myvar = 10;
console.log(myvar.name); //object
// myvar(); //function
// myvar.toUpperCase(); //string function
//therefore to tackle this we use "unknown"
var newvar = 10;
//now we have to specify explicitly: similar to typecasting
// (newvar as string).toUpperCase()
//here ts doesn't infer the value
var a;
a = 10;
a = true;
//here ts already know the type of b while declaration
//(static type checking)
var b = 10;
// b = true;
var multitype = 10;
multitype = false;
multitype = 20;
//Reasons to use union type ->
//union type still have some restrictions while working 
//intellisence support 
//? means that parameter can be passsed optionally
function add(num1, num2) {
    if (num1 === void 0) { num1 = 10; }
    if (num2) {
        return num1 + num2;
    }
    else
        return num1;
}
add(5, 10);
// add(5,'10');
add(5);
function fullName(person) {
    console.log(person.firstname + " " + person.lastname);
}
var p = {
    firstname: 'Prakhar'
};
fullName(p);
var User = /** @class */ (function () {
    function User(name) {
        this.fullname = name;
        this.lastname = 'dhanait';
    }
    User.prototype.func = function () {
        console.log('Hi in User');
    };
    return User;
}());
var emp1 = new User('Prakhar');
console.log(emp1.fullname);
var Manager = /** @class */ (function (_super) {
    __extends(Manager, _super);
    function Manager(name) {
        return _super.call(this, name) || this;
    }
    Manager.prototype.department = function () {
        console.log("Assignments " + this.lastname);
    };
    return Manager;
}(User));
var employee = /** @class */ (function (_super) {
    __extends(employee, _super);
    function employee(name) {
        return _super.call(this, name) || this;
    }
    employee.prototype.getter = function () {
        console.log("Employee " + this.fullname);
    };
    return employee;
}(Manager));
var man = new Manager('Kochar');
man.func();
man.department();
var emp = new employee('ready');
emp.getter();
//access modifiers
//  public: accessible to instance and child classes
//  private: instance and child classes can't access
//  protected: can be accessed by child classes but not by instances
