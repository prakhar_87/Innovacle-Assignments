//For Objects :
// const user: User = {
//     name: "Prakhar",
//     id: 10
// }
//For Classes :
var UserClass = /** @class */ (function () {
    function UserClass(name, id) {
        this.name = name;
        this.id = id;
    }
    return UserClass;
}());
var user1 = new UserClass('Prakhar', 10);
//To annotate parameters and return value to a function
function getData1() {
    return { name: 'Prakhar', id: 20 };
}
function getData2(user) {
    var un = user.name;
    var ui = user.id;
    console.log('In getData2');
}
var user = { name: 'prakhar', id: 20 };
getData2(user);


