export{}
//For Objects :

interface User {
    name: string;
    id: number;
}

// const user: User = {
//     name: "Prakhar",
//     id: 10
// }

//For Classes :

class UserClass {
    name: string;
    id: number;

    constructor(name: string, id: number) {
        this.name = name;
        this.id = id;
    }
}

const user1: User = new UserClass('Prakhar',10);


//To annotate parameters and return value to a function

function getData1(): User { // return type is Object defined by interface User
    return {name: 'Prakhar', id: 20};
}

function getData2(user: User) {
    let un = user.name;
    let ui = user.id;
    console.log('In getData2');
}

let user = {name: 'prakhar', id: 20};
getData2(user);



//Type matching will check subsets of the fields required

interface Point {
    x: number;
    y: number;
}

function display(p: Point) {
    console.log(`${p.x} ${p.y}`);
}

const point1 = {x: 1, y: 2};
display(point1); //works fine

const point2 = {x: 3, y: 4, z: 60};
display(point2); //works fine and doesn't print z

const point3 = {z: 20};
// display(point3); //this doesn't work since it should be super set of point













