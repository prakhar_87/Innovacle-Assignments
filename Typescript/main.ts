//treats main.ts as module instead of script
export {}   
let message = "Hello User !";
console.log(message);

let x = 10;

let consition: boolean = true;
let sum: number = 20;
let name: string = 'prakhar';

let sent: string = `My name is ${name}
Btech CSE IIITN`;

console.log(sent);

let n: null = null;
let u: undefined = undefined;

let myName: string = undefined;

//Arrays

let list1: number[] = [1,2,3];
let list2: Array<number> = [1,2,3];

let mixed: [string, number] = ['string', 10];

enum Color {Red, Green = 12, Blue};
//Red: 0 , Green: 1, Blue: 3

let c: Color = Color.Blue;
console.log(c);


let randomValue: any = 10;
randomValue = true;
randomValue = 'Prakhar';

let list3: Array<any> = ['prakhar', 23, true]
console.log(list3);

//any can be called as a funtion or an object or anything
let myvar: any = 10;
console.log(myvar.name); //object

// myvar(); //function
// myvar.toUpperCase(); //string function

//therefore to tackle this we use "unknown"

let newvar: unknown = 10;
//now we have to specify explicitly: similar to typecasting
// (newvar as string).toUpperCase()

//here ts doesn't infer the value
let a;
a = 10;
a = true;

//here ts already know the type of b while declaration
//(static type checking)
let b = 10;
// b = true;

let multitype :boolean | number = 10;
multitype = false;
multitype = 20;

//Reasons to use union type ->
//union type still have some restrictions while working 
//intellisence support 

//? means that parameter can be passsed optionally
function add(num1: number = 10, num2?: number): number { //return type
    if(num2) {
        return num1+num2;
    }
    else return num1;
}

add(5,10);
// add(5,'10');
add(5);

//optional parameters should always be after the required parameter 

//interface
interface Person {
    firstname: string;
    lastname?: string;
}

function fullName(person: Person) {
    console.log(`${person.firstname} ${person.lastname}`); 
}

let p = {
    firstname: 'Prakhar',
}

fullName(p);


class User {
    protected lastname: string
    fullname: string
    constructor(name: string) {
        this.fullname = name;
        this.lastname = 'dhanait';
    }

    func() {
        console.log('Hi in User');
        
    }
}

let emp1 = new User('Prakhar');

console.log(emp1.fullname);

class Manager extends User{
    constructor(name) {
        super(name);
    }

    department() {
        console.log(`Assignments ${this.lastname}`);
    }
}

class employee extends Manager {

    constructor(name) {
        super(name);
    }

    getter() {
        console.log(`Employee ${this.fullname}`);
    }
}

let man = new Manager('Kochar');
man.func();
man.department();

let emp = new employee('ready');
emp.getter();


//access modifiers
//  public: accessible to instance and child classes
//  private: instance and child classes can't access
//  protected: can be accessed by child classes but not by instances



















 










