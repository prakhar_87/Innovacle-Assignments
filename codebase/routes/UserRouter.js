import express from 'express';
import {createUser, validateUser, createPost, getPosts, resetPassword}  from '../services/route_handler_services/handlers.js';
import {isAuth} from '../services/authentication_service/jwtAuth.js';

const userRouter = express.Router();

userRouter.get('/', (req,res) => {
    res.json({
        message: "Welcome to the home Page",
        status: 200
    })
})

userRouter.post('/signup', createUser);

userRouter.post('/signin', validateUser);

userRouter.get('/posts', getPosts);

userRouter.post('/posts', isAuth, createPost);

userRouter.put('/sigin/resetPassword', resetPassword);

export default userRouter;