export const resMessages = {
    SUCCESS: "Succcessful !",
    CREATED: "Creates Successfully !",
    UPDATED: "Updated Successfully !",
    ERROR: "Unexpected Error Occured !"
}

export const resFlags = {
    SUCCESS: 200,
    CREATED: 201,
    ERROR: 401
}