// const Joi = require('@hapi/joi');
import Joi from '@hapi/joi';

const signupSchema = Joi.object({
    username: Joi.string().min(3).required(),
    password: Joi.string().min(8).required(),
    email   : Joi.string().email().required()
})

export { signupSchema }