// const Joi = require('@hapi/joi');
import Joi from '@hapi/joi';

const signupSchema = Joi.object({
    author: Joi.string().required(),
    title: Joi.string().required(),
    description: Joi.string().required(),
    image: Joi.string(),
    url: Joi.string(),
    isPublished: Joi.string().max(1)
})

export { signupSchema }