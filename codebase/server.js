import express from 'express';
import userRouter from './routes/UserRouter.js'
const app = express();

const port = 3002;
const location = 'localhost';
app.use(express.json());

app.use('/user', userRouter);

app.use((err, req, res, next) => {
    if(err) {
        res.json({
            message: err.message,
            satus: 404
        })
    }
})

app.listen(port, location, () => {
    console.log(`Server is up and running at http://${location}:${port}`);
})
