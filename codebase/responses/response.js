import { resMessages, resFlags } from "../constants/responses.js"

export const sendResponse = (res, data, message, status) => {
    const response = {
        message: message || resMessages.SUCCESS,
        status: status || resFlags.SUCCESS,
        data: data || "none"
    }
    res.json(response);
}

export const eroniousResponse = (err, req, res, status) => {
    const response = {
        message: err.message || resMessages.ERROR,
        status: status || resFlags.ERROR
    }
    res.json(response);
}