import bcrypt from 'bcrypt';
import db from '../connection.js';
import {generateToken} from '../authentication_service/jwtToken.js';
import { sendResponse, eroniousResponse } from '../../responses/response.js';
import { signupSchema } from '../../joi/signupModel.js';

//add user entry in the database
const createUser = async (req, res, next) => {
    let {username,email,password} = req.body;
    let secure = await bcrypt.hash(password, 10);
    req.body.password = secure;

    try {
        const check = await signupSchema.validateAsync(req.body); 
    }
    catch(err) {
        eroniousResponse(err, req, res); 
    }

    //inserting user details into the user detail table
    db.query(`INSERT INTO TB_USERS SET ?`, req.body, (err, result) => {
        if(err) eroniousResponse(err, req, res);
        db.query(`SELECT * FROM TB_USERS WHERE EMAIL = '${email}'`, (err,result) => {
            if(err) eroniousResponse(err, req, res);
            const data = {
                user_Id: result[0].user_id,
                username,
                email,
                CreatedAt: result[0].CreatedAt,
                updatedAt: result[0].UpdatedAt
            }
            sendResponse(res, data);
        })
    })
}

//validate user entry present in the database and generates jwt token
const validateUser = async (req, res, next) => {
    let {username,email,password} = req.body;
    let userData = {username, email}

    try {
        const check = await signupSchema.validateAsync(req.body); 
    }
    catch(err) {
        eroniousResponse(err, req, res); 
    }
    
    db.query(`SELECT * FROM TB_USERS WHERE EMAIl = '${email}'`, async(err, result) => {
        if(err) eroniousResponse(err, req, res);    
        let validator = await bcrypt.compare(password, result[0].password);
        if(validator) {
            const data = {
                username,
                email,
                token : generateToken(userData)
            }
            sendResponse(res, data);
        }
    })
}

const createPost = (req, res, next) => {
    db.query(`INSERT INTO TB_POST SET ?`, req.body, (err, result) => {
        if(err) eroniousResponse(err, req, res);
        db.query(`SELECT * FROM TB_POST WHERE author = '${req.body.author}'`, (err, result) => {
            if(err) eroniousResponse(err, req, res);
            else {
                const data = {
                    ...result[0]
                }                
                sendResponse(res, data);
            }
        })
    })
}

const resetPassword = async (req, res, next) => {
    const newPassword = req.body.newPassword;
    const confirmPassword = req.body.confirmPassword;
    if(newPassword === confirmPassword) {
        console.log(newPassword);
        let secure = await bcrypt.hash(newPassword, 11);
        db.query(`UPDATE TB_USERS SET password = ? where user_id = ?`, [secure, req.body._id], (err, result) => {
            if(err) eroniousResponse(err, req, res);
            sendResponse(res);
        })
    }
    else {
        err = {
            message: "Password dont Match !"
        }
        eroniousResponse(err, req, res);
    }
}

const getPosts = (req, res, next) => {
    db.query(`SELECT * FROM TB_POST`, (err, result) => {
        console.log(result);
        if(err) errorHandler(err, req, res);
        const data = {
            ...result
        }
        sendResponse(res, data);
    });
}

export { createUser, validateUser, createPost, resetPassword, getPosts};
