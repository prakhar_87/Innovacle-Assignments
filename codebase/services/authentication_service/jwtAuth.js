import jwt from 'jsonwebtoken';
import { eroniousResponse } from '../../responses/response.js';

//to handle authentication
export const isAuth = (req, res, next) => {
    const header = req.headers.authorization;
    if(header) {
        const token = header.split(" ")[1];
        jwt.verify(
            token,
            "secretkey",
            (err, decode) => {
                if(err) errorHandler(err, req, res);
                else {
                    console.log(decode);
                    // createPost(req, res, next);
                    next();
                }
            }
        )
    }
    else {
        const err = {
            message: "Unauthorized"
        }
        eroniousResponse(err, req, res, 404); 
    }
}